var Schema = mongoose.Schema;
var productoSchema = new Schema({

    nombre: String,
    descripcion: String,
    precio: Number

});

const Producto = mongoose.model('producto',productoSchema);
module.exports = Producto;