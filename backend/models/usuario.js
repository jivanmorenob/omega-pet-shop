// var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var UsuarioSchema = new Schema({

    nombre: String,
    apellido: String,
    cedula: String,
    correo: String,
    direccion: String

});

const Usuario = mongoose.model('Usuario',UsuarioSchema);
module.exports = Usuario;

