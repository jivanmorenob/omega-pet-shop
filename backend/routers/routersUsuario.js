const { Router } = require("express");
const routerUsuario = Router();

var controllerUsuarios=require('../controllers/controllerUsuarios');

// routerUsuario.get('/prueba',controllerUsuarios.saludo);

routerUsuario.get("/api/usuarios", (req, res) => {
    res.send("Funcionalidad de Usuario Activa");
});

routerUsuario.post('/',controllerUsuarios.saveUsuario);
routerUsuario.get('/buscar/:id',controllerUsuarios.buscarUsuario);
routerUsuario.get('/buscarall/:id?',controllerUsuarios.listarUsuarios);
routerUsuario.delete('/borrar/:id',controllerUsuarios.deleteUsuario);
routerUsuario.put('/actualizar/:id',controllerUsuarios.updateUsuario);


module.exports = routerUsuario;