const { Router } = require("express");
const routerProducto = Router();

var controllerProductos=require('../controllers/controllerProductos');

// routerUsuario.get('/prueba',controllerUsuarios.saludo);

routerProducto.get("/productos", (req, res) => {
    res.send("Funcionalidad de Usuario Activa");
});

routerProducto.post('/nuevo',controllerProductos.saveProducto);
routerProducto.get('/buscar/:id',controllerProductos.buscarProducto);
routerProducto.get('/buscarall/:id?',controllerProductos.listarProductos);
routerProducto.delete('/borrar/:id',controllerProductos.deleteProducto);
routerProducto.put('/actualizar/:id',controllerProductos.updateProducto);


module.exports = routerProducto;