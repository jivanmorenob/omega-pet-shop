var app = require('./app');
var mongoose =require('./conexDB/conn');
var port = 4000;

const routerUsuario = require("./routers/routersUsuario");
const routerProducto = require("./routers/routersProducto");

app.listen(port,()=>{console.log("servidor corriendo ok")})

app.get("/", (req, res) => {
    res.send("API Funcionando");
});


//rutas
app.use("/api/usuarios", routerUsuario);
app.use("/productos", routerProducto);