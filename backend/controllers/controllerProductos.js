const Producto = require("../models/producto");

//CREAR NUEVO USUARIO

const saveProducto = async (req, res) => {
    try {
        const producto = new Producto(req.body);
        await producto.save();
        res.send("Producto guardado correctamente")
    } catch (error) {
        console.error(error);
    }
}
//BUSCAR POR ID
function buscarProducto(req, res) {
    var idProducto = req.params.id;
    Producto.findById(idProducto).exec((err, result) => {
        if (err) {
            res.status(500).send({ message: 'Error al momento de ejecutar la solicitud' });
        } else {
            if (!result) {
                res.status(404).send({ message: 'El registro a buscar no se encuentra disponible' });
            } else {
                res.status(200).send({ result });
            }
        }
    });
}

function listarProductos(req, res) {
    var idProducto = req.params.idb;
    if (!idProducto) {
        var result = Producto.find({}).sort('nombre');
    } else {
        var result = Producto.find({ _id: idProducto }).sort('nombre');
    }
    result.exec(function (err, result) {
        if (err) {
            res.status(500).send({ message: 'Error al momento de ejecutar la solicitud' });
        } else {
            if (!result) {
                res.status(404).send({ message: 'El registro a buscar no se encuentra disponible' })
                    ;
            } else {
                res.status(200).send({ result });
            }
        }
    })
}

//ACTUALIZAR

const updateProducto= async (req, res) => {
    try {
        const id = req.params.id;
        const producto = req.body;
        await Producto.findByIdAndUpdate(id, producto);
        res.send("Plan actualizado correctamente");
    }catch (error) {
        console.error(error);
    }
}

//BORRAR
function deleteProducto(req, res) {
    var idProducto = req.params.id;
    Producto.findByIdAndRemove(idProducto, function (err, producto) {
        if (err) {
            return res.json(500, {
                message: 'No hemos encontrado la carrera'
            })
        }
        return res.json(producto)
    });
}

    module.exports = {
        
        saveProducto,
        buscarProducto,
        listarProductos,
        updateProducto,
        deleteProducto
    }

