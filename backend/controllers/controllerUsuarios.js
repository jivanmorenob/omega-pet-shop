const Usuario = require("../models/usuario");

function saludo(req, res) {
    res.status(200).send({
        message: 'probando una acción'
    });
}

//CREAR NUEVO USUARIO

const saveUsuario = async (req, res) => {
    try {
        const usuario = new Usuario(req.body);
        await usuario.save();
        res.send("Cliente guardado correctamente")
    } catch (error) {
        console.error(error);
    }
}

// function saveUsuario(req, res) {
//     var myUsuario = new Usuario(req.body);
//     myUsuario.save((err, result) => {
//         res.status(200).send({ message: result });
//     });
// }

//BUSCAR POR ID
function buscarUsuario(req, res) {
    var idUsuario = req.params.id;
    Usuario.findById(idUsuario).exec((err, result) => {
        if (err) {
            res.status(500).send({ message: 'Error al momento de ejecutar la solicitud' });
        } else {
            if (!result) {
                res.status(404).send({ message: 'El registro a buscar no se encuentra disponible' });
            } else {
                res.status(200).send({ result });
            }
        }
    });
}

function listarUsuarios(req, res) {
    var idUsuario = req.params.idb;
    if (!idUsuario) {
        var result = Usuario.find({}).sort('nombre');
    } else {
        var result = Usuario.find({ _id: idUsuario }).sort('nombre');
    }
    result.exec(function (err, result) {
        if (err) {
            res.status(500).send({ message: 'Error al momento de ejecutar la solicitud' });
        } else {
            if (!result) {
                res.status(404).send({ message: 'El registro a buscar no se encuentra disponible' })
                    ;
            } else {
                res.status(200).send({ result });
            }
        }
    })
}

//ACTUALIZAR

const updateUsuario= async (req, res) => {
    try {
        const id = req.params.id;
        const usuario = req.body;
        await Usuario.findByIdAndUpdate(id, usuario);
        res.send("Plan actualizado correctamente");
    }catch (error) {
        console.error(error);
    }
}

//BORRAR
function deleteUsuario(req, res) {
    var idUsuario = req.params.id;
    Usuario.findByIdAndRemove(idUsuario, function (err, usuario) {
        if (err) {
            return res.json(500, {
                message: 'No hemos encontrado la carrera'
            })
        }
        return res.json(usuario)
    });
}

    module.exports = {
        saludo,
        saveUsuario,
        buscarUsuario,
        listarUsuarios,
        updateUsuario,
        deleteUsuario
    }

